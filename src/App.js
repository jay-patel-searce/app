import React, { useState } from "react";
import Todo from "./Components/Todo/Todo";
import AddEditTodo from "./Components/AddEditTodo/AddEditTodo";
import "./App.css";

const App = () => {
	const [currentTodo, setCurrentTodo] = useState(null);

	const [todos, setTodos] = useState([
		{
			id: "1",
			title: "Learn branching ",
			description: "description 1 ",
			status: false,
		},
		{
			id: "2",
			title: "Add and commit ",
			description: "description 2 ",
			status: false,
		},
		{
			id: "3",
			title: "Make Pull Request ",
			description: "description 3 ",
			status: false,
		},
	]);

	const addTodo = (newTodo) =>
		setTodos([
			...todos,
			{
				id: todos.length + 1,
				title: newTodo.title,
				status: false,
				description: newTodo.description,
			},
		]);

	const editTodo = (newTodo) => {
		setTodos(
			todos.map((todo) => (todo.id === currentTodo.id ? newTodo : todo))
		);
		setCurrentTodo(null);
	};

	const deleteTodo = (id) => setTodos(todos.filter((todo) => todo.id !== id));

	const completeTodo = (id) => {
		setTodos(
			todos.map((todo) =>
				todo.id === id ? { ...todo, status: !todo.status } : todo
			)
		);
	};

	return (
		<>
			<h1>CRUD App</h1>

			<AddEditTodo
				updateTodo={currentTodo ? editTodo : addTodo}
				currentTodo={currentTodo}
			>
				{currentTodo ? "Update Todo" : "Add Todo" }
			</AddEditTodo>

			<div>
				{todos.map((todo) => (
					<Todo
						key={todo.id}
						todo={todo}
						editTodo={() => setCurrentTodo(todo)}
						deleteTodo={() => deleteTodo(todo.id)}
						completeTodo={() => completeTodo(todo.id)}
					/>
				))}
			</div>
		</>
	);
};

export default App;
