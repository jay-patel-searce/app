import React from "react";
import classes from "./Todo.module.css";

const Todo = ({ todo, deleteTodo, editTodo, completeTodo }) => {
	return (
		<div className={todo.status ? classes.ClosedTodo : classes.OpenTodo}>
			<h3>{todo.title}</h3>
			<p>{todo.description}</p>
			<button onClick={editTodo} className={classes.EditBtn}>
				Edit
			</button>

			<button onClick={deleteTodo} className={classes.DeleteBtn}>
				Delete
			</button>
			<button onClick={completeTodo} className={classes.CompleteBtn}>
				{todo.status ? "re-open" : "Complete"}
			</button>
		</div>
	);
};

export default Todo;
