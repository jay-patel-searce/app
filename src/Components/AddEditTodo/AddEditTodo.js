import React, { useEffect, useState } from "react";
import classes from "./AddEditTodo.module.css";

const AddEditTodo = ({ children, updateTodo, currentTodo }) => {
	const [todo, setTodo] = useState(
		currentTodo ? currentTodo : { title: "", description: "" }
	);

	useEffect(
		() => setTodo(currentTodo ? currentTodo : { title: "", description: "" }),
		[currentTodo]
	);

	return (
		<form
			className={classes.form}
			onSubmit={(event) => {
				event.preventDefault();
				updateTodo({
					...todo,
					title: todo.title,
					description: todo.description,
				});
				setTodo({ ...todo, title: "", description: "" });
			}}
		>
			<input
				required
				type="text"
				onChange={(event) => setTodo({ ...todo, title: event.target.value })}
				value={todo.title}
				placeholder="title"
			/>

			<input
				required
				type="text"
				onChange={(event) =>
					setTodo({ ...todo, description: event.target.value })
				}
				value={todo.description}
				placeholder="description"
			/>
			<button>{children}</button>
		</form>
	);
};

export default AddEditTodo;
